package com.tpms.rnc.tpms_wheel_formula;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;


public class SettingsActivity extends AppCompatActivity {

    public static final String KEY_TRUCK_FORMULA = "TruckFormula";
    public static final String KEY_TRAILER1_FORMULA = "Trailer1Formula";
    public static final String KEY_TRAILER2_FORMULA = "Trailer2Formula";
    public static final String KEY_DROP_TRAILER1 = "DropTrailer1";
    public static final String KEY_DROP_TRAILER2 = "DropTrailer2";
    public static final String KEY_USE_SCROLL = "UseScroll";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        final EditText truckFormula = (EditText) findViewById(R.id.editText_truckFormula);
        final EditText trailer1Formula = (EditText) findViewById(R.id.editText_trailer1Formula);
        final EditText trailer2Formula = (EditText) findViewById(R.id.editText_trailer2Formula);

        final CheckBox dropTrailer1 = (CheckBox) findViewById(R.id.checkBox_trailer1Enable);
        final CheckBox dropTrailer2 = (CheckBox) findViewById(R.id.checkBox_trailer2Enable);

        final CheckBox useScroll = (CheckBox) findViewById(R.id.checkBox_useScroll);

        Button apply = (Button) findViewById(R.id.button_apply);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra(KEY_TRUCK_FORMULA, truckFormula.getText().toString());
                i.putExtra(KEY_TRAILER1_FORMULA, trailer1Formula.getText().toString());
                i.putExtra(KEY_TRAILER2_FORMULA, trailer2Formula.getText().toString());

                i.putExtra(KEY_DROP_TRAILER1, dropTrailer1.isChecked());
                i.putExtra(KEY_DROP_TRAILER2, dropTrailer2.isChecked());

                i.putExtra(KEY_USE_SCROLL, useScroll.isChecked());

                setResult(RESULT_OK, i);
                finish();
            }
        });
    }
}
