package com.tpms.rnc.tpms_wheel_formula;


import android.app.Activity;
import android.content.Context;
import android.view.View;

public class STrailerTrain {

	private STruck mTruck;
	private STruck mTrailer1;
	private STruck mTrailer2;
	private Context mContext;
	private STireWidget mSTireWidget;

	public STrailerTrain(Context context) {
		this.mContext = context;
		mSTireWidget = new STireWidget(context);

		mTruck = new STruck();
		mTrailer1 = new STruck();
		mTrailer2 = new STruck();
	}

	public STruck getTruck() {
		return mTruck;
	}

	public STruck getTrailer1() {
		return mTrailer1;
	}

	public STruck getTrailer2() {
		return mTrailer2;
	}

	public int getAxleCount() {
		return mTruck.getAxleList().size() + mTrailer1.getAxleList().size() + mTrailer2.getAxleList().size();
	}

	public View getTireWidget() {
		return mSTireWidget.getTireWidget();
	}

	public void setWidgetScreenHeight(int height) {
		mSTireWidget.setScreenHeight(height);
	}

	public void setWidgetScreenWidth(int width) {
		mSTireWidget.setScreenWidth(width);
	}

	public void setEnableScroll(boolean enableScroll) {
		mSTireWidget.setEnableScroll(enableScroll, this);
	}

	public void setTirePressure(STruck STruck, int position, String pressure) {
		mSTireWidget.setTirePressure(STruck, position, pressure);
	}

	public void setTireTemperature(STruck STruck, int position, String temperature) {
		mSTireWidget.setTireTemperature(STruck, position, temperature);
	}

	public void setAlarm(STruck STruck, int position, boolean onUse) {
		mSTireWidget.setAlarm(STruck, position, onUse);
	}

	public void updateTireWidget() {
		mSTireWidget.clearWheelFormula();
		mSTireWidget.addToWidget(this);
	}

	public void setEnable(boolean enable) {
		mSTireWidget.setEnable(mTruck.getWheelLayouts(), enable);
		mSTireWidget.setEnable(mTrailer1.getWheelLayouts(), enable);
		mSTireWidget.setEnable(mTrailer2.getWheelLayouts(), enable);
	}

	public void setEmptyScreen(boolean onUse) {
		mSTireWidget.setEmptyScreen(onUse);
		Activity activity = (Activity) mContext;
		activity.setContentView(mSTireWidget.getTireWidget());
	}

	public void dropTruck() {
		mTruck.getAxleList().clear();
	}

	public void dropTrailer1() {
		if (mTrailer1.getAxleList().size()>0) {
			mTrailer1.getAxleList().clear();
			mTrailer1.setTruckNumber("");
			updateTireWidget();
		}
	}

	public void dropTrailer2() {
		if (mTrailer2.getAxleList().size()>0) {
			mTrailer2.getAxleList().clear();
			mTrailer2.setTruckNumber("");
			updateTireWidget();
		}
	}

	public boolean isFormula_2x2() {
		boolean result = false;

		if (mTruck.getAxleList().size() > 1) {
			result = (getAxleCount() == 2 & (mTruck.getAxleList().get(0).isDoubleWheel() + mTruck.getAxleList().get(1).isDoubleWheel() == 0));
		}

		return result;
	}

	public boolean isScrollEnable() {
		return mSTireWidget.isScrollEnable();
	}

	public void setTruckNumber(STruck truck, String number) {
		truck.setTruckNumber(number);
		if (mSTireWidget != null)
			mSTireWidget.updateTrailersNumber(this);
	}
}
