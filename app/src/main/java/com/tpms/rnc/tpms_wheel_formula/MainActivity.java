package com.tpms.rnc.tpms_wheel_formula;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_SETTINGS = 0;

    private String mTruckFormula;
    private String mTrailer1Formula;
    private String mTrailer2Formula;

    STrailerTrain STrailerTrain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar();

        STrailerTrain = new STrailerTrain(this);

        setContentView(STrailerTrain.getTireWidget());

        int[] truckAxles = new int[]{0,1};
        STrailerTrain.getTruck().setAxleFormula(truckAxles);

        int[] traileAxles = new int[]{0,0,0};
        STrailerTrain.getTrailer1().setAxleFormula(traileAxles);

        //int[] trailer2Axles = new int[]{1};
        //STrailerTrain.getTrailer2().setAxleFormula(trailer2Axles);
        //STrailerTrain.setTruckNumber(STrailerTrain.getTrailer2(),"5555");

        //STrailerTrain.dropTrailer2();

        final LinearLayout parentLayout = (LinearLayout) findViewById(R.id.wheel_formula);
        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                parentLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                STrailerTrain.setWidgetScreenHeight(parentLayout.getHeight());
                STrailerTrain.setWidgetScreenWidth(parentLayout.getWidth());
                STrailerTrain.setEnableScroll(true);
                STrailerTrain.updateTireWidget();

                STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 0, "21.65");
                STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 0, "16.85");
                STrailerTrain.setAlarm(STrailerTrain.getTruck(), 0, true);

                STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 1, "0.0");
                STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 1, "0.0");


                STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 2, "21.2");
                STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 2, "7.9");


                STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 3, "17.5");
                STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 3, "8.3");

                //STrailerTrain.setOldInformation();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings_menu :
                Intent i = new Intent(this, SettingsActivity.class);
                startActivityForResult(i, REQUEST_SETTINGS);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;

        if (requestCode == REQUEST_SETTINGS & resultCode == RESULT_OK) {
            mTruckFormula = data.getStringExtra(SettingsActivity.KEY_TRUCK_FORMULA);
            if (mTruckFormula != null) {
                int[] truckAxles = new int[mTruckFormula.length()];
                for (int i = 0; i < truckAxles.length; i++) {
                    truckAxles[i] = Integer.parseInt(String.valueOf(mTruckFormula.charAt(i)));
                    STrailerTrain.getTruck().setAxleFormula(truckAxles);

                }
            }

            mTrailer1Formula = data.getStringExtra(SettingsActivity.KEY_TRAILER1_FORMULA);
            if (mTrailer1Formula != null) {
                int[] trailer1Axles = new int[mTrailer1Formula.length()];
                for (int i = 0; i < trailer1Axles.length; i++) {
                    trailer1Axles[i] = Integer.parseInt(String.valueOf(mTrailer1Formula.charAt(i)));
                    STrailerTrain.getTrailer1().setAxleFormula(trailer1Axles);

                }
            }

            mTrailer2Formula = data.getStringExtra(SettingsActivity.KEY_TRAILER2_FORMULA);
            if (mTrailer2Formula != null) {
                int[] trailer2Axles = new int[mTrailer2Formula.length()];
                for (int i = 0; i < trailer2Axles.length; i++) {
                    trailer2Axles[i] = Integer.parseInt(String.valueOf(mTrailer2Formula.charAt(i)));
                    STrailerTrain.getTrailer2().setAxleFormula(trailer2Axles);

                }
            }

            boolean dropTrailer1 = data.getBooleanExtra(SettingsActivity.KEY_DROP_TRAILER1, false);
            if (dropTrailer1)
                STrailerTrain.dropTrailer1();

            boolean dropTrailer2 = data.getBooleanExtra(SettingsActivity.KEY_DROP_TRAILER2, false);
            if (dropTrailer2)
                STrailerTrain.dropTrailer2();

            boolean useScroll = data.getBooleanExtra(SettingsActivity.KEY_USE_SCROLL, false);
                STrailerTrain.setEnableScroll(useScroll);

            STrailerTrain.updateTireWidget();

            STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 0, "21.65");
            STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 0, "16.85");
            STrailerTrain.setAlarm(STrailerTrain.getTruck(), 0, true);

            STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 1, "0.0");
            STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 1, "0.0");


            STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 2, "21.2");
            STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 2, "7.9");


            STrailerTrain.setTirePressure(STrailerTrain.getTruck(), 3, "17.5");
            STrailerTrain.setTireTemperature(STrailerTrain.getTruck(), 3, "8.3");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
