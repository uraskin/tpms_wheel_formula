package com.tpms.rnc.tpms_wheel_formula;

public class STruckAxle {

    private int mDoubleWheel;
    private int mWheelCount;
    private STruck mSTruck;
    private boolean mDrawSeparator = false;

    public STruckAxle(int doubleWheel, STruck STruck, int index) {
        this.mDoubleWheel = doubleWheel;
        this.mSTruck = STruck;

        if (doubleWheel == 0)
            mWheelCount = 2;
        else
            mWheelCount = 4;

        if (index == 0) {
            mDrawSeparator = true;
        }
    }

    public STruck getTruck() {
        return mSTruck;
    }

    public int isDoubleWheel() {
        return mDoubleWheel;
    }

    public int getWheelCount() {
        return mWheelCount;
    }

    public boolean isDrawSeparator() {
        return mDrawSeparator;
    }
}
