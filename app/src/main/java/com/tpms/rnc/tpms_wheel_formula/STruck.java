package com.tpms.rnc.tpms_wheel_formula;

import android.widget.LinearLayout;

import java.util.ArrayList;

public class STruck {

    private ArrayList<STruckAxle> mSTruckAxleList;
    private ArrayList<LinearLayout> mWheelLayouts;

    private String mTruckNumber;

    public STruck() {
        mSTruckAxleList = new ArrayList<STruckAxle>();
        mWheelLayouts = new ArrayList<LinearLayout>();
    }

    public void setAxleFormula(int[] axles) {
        mSTruckAxleList = new ArrayList<STruckAxle>();

        for (int i = 0; i < axles.length; i++) {
            mSTruckAxleList.add(new STruckAxle(axles[i], this, i));
        }
    }

    public ArrayList<STruckAxle> getAxleList() {
        return mSTruckAxleList;
    }

    public String getTruckNumber() {
        return mTruckNumber;
    }

    public void setTruckNumber(String mTruckNumber) {
        this.mTruckNumber = mTruckNumber;
    }

    public void addWheelLayoutToList(LinearLayout wheelLayout) {
        mWheelLayouts.add(wheelLayout);
    }

    public ArrayList<LinearLayout> getWheelLayouts() {
        return mWheelLayouts;
    }
}
