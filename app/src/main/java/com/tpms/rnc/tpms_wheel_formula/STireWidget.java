package com.tpms.rnc.tpms_wheel_formula;


import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.lb.auto_fit_textview.AutoResizeTextView;


public class STireWidget extends LinearLayout {

	private Context mContext;
	private LayoutInflater layoutInflater;

	private boolean ENABLE_SCROLL = false;      // используем скрол или ресайз
	private int SEPARATOR_VERTICAL = 20;        // ширина вертикального разделителя
	private int SEPARATOR_HORIZONTAL = 0;       // ширина горизонтального разделителя
	private int PARENT_LAYOUT_MARGIN_ALL = 2;   // все отступы от края для родительского layout
	private int WHEEL_LAYOUT_MARGIN_ALL = 2;    // все отступы между виджетами-плитками

	private boolean formula_2x2 = false;
	//private boolean mLandscapeMode = false;
	private boolean widgetEnable = false;
	private static int WHEEL_COUNT = 4;

	private int offset_width;
	private int offset_height;
	private int mScreenWidth;
	private int mScreenHeight;
	private int mWheelItemSize;

	private ArrayList<STruckAxle> mSTruckAxleList;
	private TextView mTrailer1Number;
	private TextView mTrailer2Number;
	private LinearLayout wheel_formula;
	private View mTireWidget;


	public STireWidget(Context context) {
		super(context);
		this.mContext = context;
		setWillNotDraw(true);

		mSTruckAxleList = new ArrayList<STruckAxle>();

		layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mTireWidget = layoutInflater.inflate(R.layout.tire_widget, this, false);
		initTireWidget();
	}

	public void setScreenWidth(int mScreenWidth) {
		this.mScreenWidth = mScreenWidth;
	}

	public void setScreenHeight(int mScreenHeight) {
		this.mScreenHeight = mScreenHeight;
	}

	public void setEnableScroll(boolean ENABLE_SCROLL, STrailerTrain STrailerTrain) {
		this.ENABLE_SCROLL = ENABLE_SCROLL;
		formula_2x2 = STrailerTrain.isFormula_2x2();
		initTireWidget();
	}

	public void initTireWidget() {
		View baseLayout;
		if (ENABLE_SCROLL & !formula_2x2)
			baseLayout = layoutInflater.inflate(R.layout.wheel_formula, this, false);
		else
			baseLayout = layoutInflater.inflate(R.layout.wheel_formula_no_scroll, this, false);

		LinearLayout tireWidget = (LinearLayout) mTireWidget.findViewById(R.id.tire_widget);
		tireWidget.removeAllViews();
		tireWidget.addView(baseLayout);

		wheel_formula = (LinearLayout) mTireWidget.findViewById(R.id.wheel_formula);
	}

	public void addToWidget(STrailerTrain mSTrailerTrain) {
		mSTruckAxleList.clear();
		formula_2x2 = mSTrailerTrain.isFormula_2x2();
		clearWheelLayoutList(mSTrailerTrain);
		SEPARATOR_HORIZONTAL = 0;

		boolean landscapeMode = mScreenWidth > mScreenHeight;

		int axleCount = mSTrailerTrain.getAxleCount();

		if (axleCount == 0)         // если осей нет, то выводим пустой экран
			return;

		mWheelItemSize = 0;

		if (mSTrailerTrain.getTruck() != null) {
			for (STruckAxle STruckAxle : mSTrailerTrain.getTruck().getAxleList()) {
				mSTruckAxleList.add(STruckAxle);
			}
		}
		if (mSTrailerTrain.getTrailer1().getAxleList().size() != 0) {
			for (STruckAxle STruckAxle : mSTrailerTrain.getTrailer1().getAxleList()) {
				mSTruckAxleList.add(STruckAxle);
			}
			SEPARATOR_HORIZONTAL += 20;
		}
		if (mSTrailerTrain.getTrailer2().getAxleList().size() != 0) {
			for (STruckAxle STruckAxle : mSTrailerTrain.getTrailer2().getAxleList()) {
				mSTruckAxleList.add(STruckAxle);
			}
			SEPARATOR_HORIZONTAL += 20;
		}

		if (formula_2x2)
			WHEEL_COUNT = 2;
		else
			WHEEL_COUNT = 4;

		offset_width = SEPARATOR_VERTICAL + PARENT_LAYOUT_MARGIN_ALL + WHEEL_LAYOUT_MARGIN_ALL * WHEEL_COUNT*2;
		offset_height = PARENT_LAYOUT_MARGIN_ALL + WHEEL_LAYOUT_MARGIN_ALL * axleCount*2 + SEPARATOR_HORIZONTAL;

		int offset_height_px = convertDpToPixel(offset_height, getContext());
		int offset_width_px = convertDpToPixel(offset_width, getContext());

		int maxElementHeight = (mScreenHeight - offset_height_px)/axleCount;
		int maxElementWidth = (mScreenWidth - offset_width_px)/WHEEL_COUNT;

		if (ENABLE_SCROLL & !formula_2x2) {
			mWheelItemSize = maxElementWidth;
			ScrollView.LayoutParams wheelFormulaParams = new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.MATCH_PARENT);
			wheelFormulaParams.gravity = Gravity.TOP;
			wheel_formula.setLayoutParams(wheelFormulaParams);
		} else {
			mWheelItemSize = Math.min(maxElementHeight, maxElementWidth);
			LayoutParams wheelFormulaParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			wheelFormulaParams.gravity = Gravity.CENTER;
			wheel_formula.setLayoutParams(wheelFormulaParams);
		}
		wheel_formula.setGravity(Gravity.CENTER);

		for (int i = 0; i < mSTruckAxleList.size(); i++) {
			STruckAxle STruckAxle = mSTruckAxleList.get(i);

			if (STruckAxle.getTruck() != mSTrailerTrain.getTruck() & STruckAxle.isDrawSeparator()) {
				LinearLayout numberSeparator = (LinearLayout) layoutInflater.inflate(R.layout.truck_number, null);
				numberSeparator.inflate(mContext, R.layout.truck_number, null);

				if (STruckAxle.getTruck() == mSTrailerTrain.getTrailer1()) {
					mTrailer1Number = (TextView) numberSeparator.findViewById(R.id.truckTextSeparator);
				} else if (STruckAxle.getTruck() == mSTrailerTrain.getTrailer2()) {
					mTrailer2Number = (TextView) numberSeparator.findViewById(R.id.truckTextSeparator);
				}

				updateTrailersNumber(mSTrailerTrain);
				wheel_formula.addView(numberSeparator);
			}

			LinearLayout axleLayout = new LinearLayout(mContext);
			LayoutParams axleParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);;
			if (mScreenWidth < mScreenHeight) {           //  portrait mode
				axleLayout.setOrientation(LinearLayout.HORIZONTAL);
				axleParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				if (formula_2x2 & i == 0) {
					axleParams.gravity = Gravity.TOP | Gravity.CENTER;
					axleParams.weight = 1;
					axleLayout.setGravity(Gravity.TOP);
				} else if (formula_2x2 & i == 1) {
					axleParams.gravity = Gravity.BOTTOM | Gravity.CENTER;
					axleParams.weight = 1;
					axleLayout.setGravity(Gravity.BOTTOM);
				}
			} else if(formula_2x2 & landscapeMode) {     // landscape mode only for 2x2
				axleLayout.setOrientation(LinearLayout.HORIZONTAL);
				axleParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				wheel_formula.setOrientation(VERTICAL);

				if (formula_2x2 & i == 0) {
					//axleParams.gravity = Gravity.LEFT|Gravity.CENTER;
					//axleParams.weight = 1;
					//axleLayout.setGravity(Gravity.LEFT);
				}

				if (formula_2x2 & i == 1) {
					//axleParams.gravity = Gravity.RIGHT|Gravity.CENTER;
					//axleParams.weight = 1;
					//axleLayout.setGravity(Gravity.RIGHT);
				}
			}
			axleLayout.setLayoutParams(axleParams);

			for (int j = 0; j < WHEEL_COUNT; j++) {
				//Tire tire = Tire.getTireByIndex(i, j, truck.getTireList());

				// после второго колеса выводим пустую область - разделитель
				if (j==2) {
					LinearLayout separator_layout = new LinearLayout(mContext);
					LayoutParams separator_params = new LayoutParams(SEPARATOR_VERTICAL, ViewGroup.LayoutParams.MATCH_PARENT);
					separator_layout.setLayoutParams(separator_params);
					axleLayout.addView(separator_layout);
				}

				LinearLayout wheel_layout = (LinearLayout) layoutInflater.inflate(R.layout.wheel_item, null);
				wheel_layout.inflate(mContext, R.layout.wheel_item, null);
				LayoutParams params = new LayoutParams(mWheelItemSize, mWheelItemSize);
				params.setMargins(WHEEL_LAYOUT_MARGIN_ALL, WHEEL_LAYOUT_MARGIN_ALL, WHEEL_LAYOUT_MARGIN_ALL, WHEEL_LAYOUT_MARGIN_ALL);
				wheel_layout.setLayoutParams(params);

				if (formula_2x2 || !(STruckAxle.getWheelCount() == 2 & (j == 1 | j == 2)))
					STruckAxle.getTruck().addWheelLayoutToList(wheel_layout);

				// скрываем колеса, если это не спарка
				if (STruckAxle.isDoubleWheel() == 0 &(j==1 || j ==2) & !formula_2x2)
					wheel_layout.setVisibility(View.INVISIBLE);

				axleLayout.addView(wheel_layout);

				if (j == 0 & formula_2x2 & landscapeMode) {
					LinearLayout separator_layout = new LinearLayout(mContext);
					LayoutParams separator_params = new LayoutParams(mScreenWidth - ((WHEEL_LAYOUT_MARGIN_ALL+mWheelItemSize)*2), ViewGroup.LayoutParams.MATCH_PARENT);
					separator_layout.setLayoutParams(separator_params);
					axleLayout.addView(separator_layout);
				}

			}

			wheel_formula.addView(axleLayout);
		}
	}

	public static void setBlinkAnimation(View view) {
		Animation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(400);
		anim.setStartOffset(20);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);
		view.startAnimation(anim);
	}

	public View getTireWidget() {
		return mTireWidget;
	}

	public void setTirePressure(STruck STruck, int position, String pressure) {
		if (position<STruck.getWheelLayouts().size()) {
			LinearLayout wheel_layout = STruck.getWheelLayouts().get(position);

			if (wheel_layout != null) {
				AutoResizeTextView pressureValue = (AutoResizeTextView) wheel_layout.findViewById(R.id.pressureValue);
				pressureValue.setText(pressure);
				pressureValue.setMaxLines(1);
			}
		}
	}

	public void setTireTemperature(STruck STruck, int position, String temperature) {
		if (position<STruck.getWheelLayouts().size()) {
		LinearLayout wheel_layout = STruck.getWheelLayouts().get(position);
		
			if (wheel_layout != null) {
				AutoResizeTextView temperatureValue = (AutoResizeTextView) wheel_layout.findViewById(R.id.temperatureValue);            temperatureValue.setText(temperature);

				temperatureValue.setMaxLines(1);
			}
		}
	}

	public void setAlarm(STruck STruck, int position, boolean onUse) {
		if (position<STruck.getWheelLayouts().size()) {
			LinearLayout wheel_layout = STruck.getWheelLayouts().get(position);

			AutoResizeTextView pressureValue = (AutoResizeTextView) wheel_layout.findViewById(R.id.pressureValue);
			pressureValue.setMaxLines(1);

			AutoResizeTextView temperatureValue = (AutoResizeTextView) wheel_layout.findViewById(R.id.temperatureValue);
			temperatureValue.setMaxLines(1);

			if (onUse) {
				setBlinkAnimation(pressureValue);
				setBlinkAnimation(temperatureValue);

				pressureValue.setTextColor(Color.parseColor("#FFFF1E1E"));
				temperatureValue.setTextColor(Color.parseColor("#FFFF1E1E"));
			} else {
				pressureValue.clearAnimation();
				temperatureValue.clearAnimation();

				if (widgetEnable) {
					pressureValue.setTextColor(Color.parseColor("#bbff00"));
					temperatureValue.setTextColor(Color.parseColor("#bbff00"));
				}
				else {
					pressureValue.setTextColor(Color.parseColor("#bebebe"));
					temperatureValue.setTextColor(Color.parseColor("#bebebe"));
				}
			}
		}
	}

	public void setEnable(ArrayList<LinearLayout> wheel_layouts, boolean enable) {
		widgetEnable = enable;
		for (LinearLayout wheel_layout : wheel_layouts) {
			TextView temperatureValue = (TextView) wheel_layout.findViewById(R.id.temperatureValue);
			setTextViewTextColor(temperatureValue);

			TextView pressureValue = (TextView) wheel_layout.findViewById(R.id.pressureValue);
			setTextViewTextColor(pressureValue);
		}
	}

	public void setTextViewTextColor(TextView textView) {
		if (widgetEnable) {
			textView.setTextColor(Color.parseColor("#bbff00"));
		}
		else {
			textView.setTextColor(Color.parseColor("#bebebe"));
		}

	}

	public void clearWheelFormula() {
		wheel_formula.removeAllViews();
	}

	public void clearWheelLayoutList(STrailerTrain trailerTrain) {
		trailerTrain.getTruck().getWheelLayouts().clear();
		trailerTrain.getTrailer1().getWheelLayouts().clear();
		trailerTrain.getTrailer2().getWheelLayouts().clear();
	}

	public void setEmptyScreen(boolean onUse) {
		mTireWidget = layoutInflater.inflate(R.layout.wheel_formula_empty, this, false);
	}

	public void checkPCRCondition () {

	}

	public boolean isScrollEnable() {
		return ENABLE_SCROLL;
	}

	public void updateTrailersNumber(STrailerTrain sTrailerTrain) {
		if (mTrailer1Number != null)
			mTrailer1Number.setText(sTrailerTrain.getTrailer1().getTruckNumber());

		if (mTrailer2Number != null)
			mTrailer2Number.setText(sTrailerTrain.getTrailer2().getTruckNumber());
	}

	public static int convertPixelsToDp(int px, Context context){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		int dp = px / ((int)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
		return dp;
	}

	public static int convertDpToPixel(int dp, Context context){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		int px = dp * ((int)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
		return px;
	}
}
